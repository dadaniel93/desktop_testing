StopIfFailed "No"
ManualRecovery "No"
Timeout "10"
Set "BROWSER" "Edge"
Set "PLAYER" "JW Player"
Set "TEST" "RML Regression"
SendStartNotification "TEST: {TEST}  BROWSER: {BROWSER} PLAYER: {PLAYER} "
Set "ROOT_FOLDER" "C:\Users\NY-QA-TEST\Documents\Automation\EyeAutomate\scripts\EyeScripts"
Repeat "{ROOT_FOLDER}\Master.csv"
	Call "{ROOT_FOLDER}\RML_WEB_TEMPLATE.txt"
	StepDelay "5000"
	
EndRepeat
SendEndNotification  "TEST: {TEST}  BROWSER: {BROWSER} PLAYER: {PLAYER} "
