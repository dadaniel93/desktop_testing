package plugin;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import eyeserver.Action;
import eyeserver.AppState;
import eyeserver.DoubleClickAction;
import eyeserver.EyeServer;
import eyeserver.LeftClickAction;
import eyeserver.MoveAction;
import eyeserver.PasteAction;
import eyeserver.Path;
import eyeserver.RightClickAction;
import eyeserver.TypeAction;

public class SuggestClones
{
	public List<Action> getSuggestions(AppState stateTree, AppState currentState)
	{
		List<Action> suggestions=new ArrayList<Action>();
		List<Action> actions=currentState.getActions();
		List<Point> locations=new ArrayList<Point>();
		List<String> texts=new ArrayList<String>();

		for(Action a:actions)
		{
			if(a instanceof MoveAction)
			{
				Point location=((MoveAction)a).getLocation();
				locations.add(location);
			}
			else if(a instanceof TypeAction)
			{
				String text=((TypeAction)a).getText();
				texts.add(text);
			}
			else if(a instanceof PasteAction)
			{
				String text=((PasteAction)a).getText();
				texts.add(text);
			}
		}
		
		// Get clones and mutations of current action path
		Path currentPath=stateTree.getCurrentPath();
		List<Action> pathActions=currentPath.getActions();
		List<Action> clonesAndMutations=stateTree.getClonesAndMutations(pathActions);

		for(Action action:clonesAndMutations)
		{
			AppState nextState=action.getNextState();
			for(Action nextStateAction:nextState.getActions())
			{
				if(nextStateAction instanceof MoveAction)
				{
					Point location=((MoveAction)nextStateAction).getLocation();
					if(!isOverlapping(location, locations))
					{
						// Does not overlap any existing actions
						MoveAction clone=(MoveAction)clone(nextStateAction);
						clone.setComment(EyeServer.translateCommand("SinceSimilarScenario"));
						suggestions.add(clone);
						locations.add(location);
					}
				}
				else if(nextStateAction instanceof TypeAction)
				{
					String text=((TypeAction)nextStateAction).getText();
					if(!containsText(text, texts))
					{
						// Does not overlap any existing actions
						TypeAction clone=(TypeAction)clone(nextStateAction);
						clone.setComment(EyeServer.translateCommand("SinceSimilarScenario"));
						suggestions.add(clone);
						texts.add(text);
					}
				}
				else if(nextStateAction instanceof PasteAction)
				{
					String text=((PasteAction)nextStateAction).getText();
					if(!containsText(text, texts))
					{
						// Does not overlap any existing actions
						PasteAction clone=(PasteAction)clone(nextStateAction);
						clone.setComment(EyeServer.translateCommand("SinceSimilarScenario"));
						suggestions.add(clone);
						texts.add(text);
					}
				}
				if(suggestions.size()>=5)
				{
					return suggestions;
				}
			}
		}

		return suggestions;
	}

	private boolean isOverlapping(Point location, List<Point> locations)
	{
		for(Point point:locations)
		{
			if(isOverlapping(location, point, 20))
			{
				return true;
			}
		}
		return false;
	}

	private boolean containsText(String text, List<String> texts)
	{
		for(String t:texts)
		{
			if(t.equals(text))
			{
				return true;
			}
		}
		return false;
	}
	
	private boolean isOverlapping(Point point1, Point point2, int overlappingDistance)
	{
		int distance=(int)point1.distance(point2);
		if(distance<=overlappingDistance)
		{
			return true;
		}
		return false;
	}
	
	private Action clone(Action original)
	{
		if(original instanceof LeftClickAction)
		{
			return new LeftClickAction((LeftClickAction)original);
		}
		else if(original instanceof RightClickAction)
		{
			return new RightClickAction((RightClickAction)original);
		}
		else if(original instanceof DoubleClickAction)
		{
			return new DoubleClickAction((DoubleClickAction)original);
		}
		else if(original instanceof MoveAction)
		{
			return new MoveAction((MoveAction)original);
		}
		else if(original instanceof TypeAction)
		{
			return new TypeAction((TypeAction)original);
		}
		else if(original instanceof PasteAction)
		{
			return new PasteAction((PasteAction)original);
		}
		return null;
	}
}
