package custom.selenium;

import java.util.Properties;

import eyeautomate.CustomCommand;
import eyeautomate.ScriptRunner;

public class For
{
	private static final String COMMAND="For";
	private static final String ICON_FILENAME="icons/while2.png";
	ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: For XPath/CSS");
			return false;
		}

		String xpath=commandParameters[0].trim();
		CustomCommand command=new CustomCommand(xpath);
		scriptRunner.setBlockCommand(command);
		scriptRunner.increaseCommandLevel();
		return true;
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter XPath or CSS>\"\nEndFor";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Click for each XPath/CSS result<br/>Usage:<br/>"+COMMAND+" XPath/CSS</html>";
	}

	public String getBeginCommand()
	{
		return "For";
	}

	public String getEndCommand()
	{
		return "EndFor";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}
}
