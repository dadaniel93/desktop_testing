package custom.selenium;

import eyeautomate.ScriptRunner;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import java.util.Properties;

//import org.openqa.selenium.ie.InternetExplorerDriver;

public class OpenBrowser
{
	private static final String COMMAND="OpenBrowser";
	private static final String ICON_FILENAME="icons/folder2.png";
	private WebDriver webDriver=null;
	ScriptRunner scriptRunner=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Browser(Firefox, Chrome or Edge)");
			return false;
		}

		if(isBrowserOpen())
		{
			// A browser is already open
			return true;
		}
		else
		{
			// Open a new browser
			try
			{
				String browser=commandParameters[0].trim();
				String osName = System.getProperty("os.name");
				boolean isWin = osName.startsWith("Windows");

				if("firefox".equalsIgnoreCase(browser))
				{
					String driverName=getGeckoDriverName();
					if(driverName!=null)
					{
						File driverFile=new File(getPath(), driverName);
						String driverPath=driverFile.getAbsolutePath();
						System.setProperty("webdriver.gecko.driver", driverPath);
						webDriver=new FirefoxDriver();
						scriptRunner.setWebDriver(webDriver);
						return true;
					}
					else
					{
						scriptParameters.put("Error", "No driver");
						return false;
					}
				}
				else if("chrome".equalsIgnoreCase(browser))
				{
					String driverName=getSeleniumDriverName();
					if(driverName!=null)
					{
						File driverFile=new File(getPath(), driverName);
						String driverPath=driverFile.getAbsolutePath();
						System.setProperty("webdriver.chrome.driver", driverPath);
						webDriver=new ChromeDriver();
						scriptRunner.setWebDriver(webDriver);
						return true;
					}
					else
					{
						scriptParameters.put("Error", "No driver");
						return false;
					}
				}
				else if("edge".equalsIgnoreCase(browser) && isWin)
				{
					//File driverFile=new File(getPath(), "bin/MicrosoftWebDriver.exe");
					File driverFile=new File("C:\\Windows\\system32\\MicrosoftWebDriver.exe");
					String driverPath=driverFile.getAbsolutePath();
					//System.setProperty("webdriver.ie.driver", driverPath);
					System.setProperty("webdriver.edge.driver", driverPath);
					//webDriver=new InternetExplorerDriver();
					webDriver=new EdgeDriver();
					scriptRunner.setWebDriver(webDriver);
					return true;
				}
				else
				{
					scriptParameters.put("Error", "Unsupported browser");
					return false;
				}
			}
			catch(Exception e)
			{
				scriptParameters.put("Error", "Exception: "+e.toString());
				return false;
			}
		}
	}
	
	private String getSeleniumDriverName()
	{
		String osName = System.getProperty("os.name");
		boolean isWin = osName.startsWith("Windows");
		boolean isOSX = osName.startsWith("Mac");
		boolean isLinux = osName.indexOf("nux")>=0;
		if(isWin)
		{
			return "bin/chromedriver.exe";
		}
		else if(isOSX)
		{
			return "bin/chromedriver_mac";
		}
		else if(isLinux)
		{
			return "bin/chromedriver_linux";
		}
		else
		{
			return null;
		}
	}
	
	private String getGeckoDriverName()
	{
		String osName = System.getProperty("os.name");
		boolean isWin = osName.startsWith("Windows");
		boolean isOSX = osName.startsWith("Mac");
		boolean isLinux = osName.indexOf("nux")>=0;
		if(isWin)
		{
			return "bin/geckodriver.exe";
		}
		else if(isOSX)
		{
			return "bin/geckodriver_mac";
		}
		else if(isLinux)
		{
			return "bin/geckodriver_linux";
		}
		else
		{
			return null;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Select browser=Chrome|Firefox|Edge>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Opens a browser<br/>Usage:<br/>"+COMMAND+" Firefox/Chrome/Edge</html>";
	}

	private String getPath()
	{
		String path = OpenBrowser.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String decodedPath = path;
		try
		{
			decodedPath = URLDecoder.decode(path, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			return null;
		}

		String absolutePath = decodedPath.substring(0, decodedPath.lastIndexOf("/")) + "/";
		return absolutePath;
	}

	private boolean isBrowserOpen()
	{
		if(webDriver==null)
		{
			return false;
		}
		try
		{
			webDriver.getCurrentUrl();
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
