package custom.selenium;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import eyeautomate.ScriptRunner;

public class OpenRemoteBrowser
{
	private static final String COMMAND="OpenRemoteBrowser";
	private static final String ICON_FILENAME="icons/folder2.png";
	private WebDriver webDriver=null;
	ScriptRunner scriptRunner=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<2)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Browser(firefox, chrome, edge or internetexplorer)");
			return false;
		}

		if(isBrowserOpen())
		{
			// A browser is already open
			return true;
		}
		else
		{
			// Open a new browser
			try
			{
				String browser=commandParameters[0].trim();
				String osName = System.getProperty("os.name");
				boolean isWin = osName.startsWith("Windows");
				if("firefox".equalsIgnoreCase(browser))
				{
					webDriver = new RemoteWebDriver(DesiredCapabilities.firefox());
					scriptRunner.setWebDriver(webDriver);
					return true;
				}
				else if("chrome".equalsIgnoreCase(browser))
				{
					webDriver = new RemoteWebDriver(DesiredCapabilities.chrome());
					scriptRunner.setWebDriver(webDriver);
					return true;
				}
				else if("edge".equalsIgnoreCase(browser) && isWin)
				{
					webDriver = new RemoteWebDriver(DesiredCapabilities.edge());
					scriptRunner.setWebDriver(webDriver);
					return true;
				}
				else if("internetexplorer".equalsIgnoreCase(browser) && isWin)
				{
					webDriver = new RemoteWebDriver(DesiredCapabilities.internetExplorer());
					scriptRunner.setWebDriver(webDriver);
					return true;
				}
				else
				{
					scriptParameters.put("Error", "Unsupported browser");
					return false;
				}
			}
			catch(Exception e)
			{
				scriptParameters.put("Error", "Exception: "+e.toString());
				return false;
			}
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Select browser=Chrome|Firefox|Edge|InternetExplorer>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Opens a remote browser<br/>Usage:<br/>"+COMMAND+" Firefox/Chrome/Edge/InternetExplorer</html>";
	}

	private boolean isBrowserOpen()
	{
		if(webDriver==null)
		{
			return false;
		}
		try
		{
			webDriver.getCurrentUrl();
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
