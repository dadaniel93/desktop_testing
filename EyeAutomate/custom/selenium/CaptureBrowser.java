package custom.selenium;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;

import eyeautomate.ScriptRunner;

public class CaptureBrowser
{
	private static final String COMMAND="CaptureBrowser";
	private static final String ICON_FILENAME="icons/capture2.png";
	private WebDriver webDriver=null;
	private ScriptRunner scriptRunner=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(webDriver==null)
		{
			scriptParameters.put("Error", "No browser open");
			return false;
		}

		try
		{
			WebDriver augmentedDriver = new Augmenter().augment(webDriver);
      File source = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
      String path = "images/" + source.getName();
      copyFile(source, new File(path));
  		scriptRunner.setParameter("CapturedImage", path);
			return true;
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "CapturedImage"};
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Takes a capture of the browser content<br/>Usage:<br/>"+COMMAND+"</html>";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}

	private boolean copyFile(File oldLocation, File newLocation) throws IOException
	{
		if(oldLocation.exists())
		{
			BufferedInputStream reader = new BufferedInputStream(new FileInputStream(oldLocation));
			BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(newLocation, false));
			try
			{
				byte[] buff = new byte[8192];
				int numChars;
				while ((numChars = reader.read(buff, 0, buff.length)) != -1)
				{
					writer.write(buff, 0, numChars);
				}
			}
			catch(IOException ex)
			{
				return false;
			}
			finally
			{
				try
				{
					if(reader != null)
					{
						writer.close();
						reader.close();
					}
				}
				catch(IOException ex)
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
		return true;
	}
}
