package custom.selenium;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import eyeautomate.ScriptRunner;

public class GetCssValue
{
	private static final String COMMAND="GetCssValue";
	private static final String ICON_FILENAME="icons/question2.png";
	private WebDriver webDriver=null;
	ScriptRunner scriptRunner=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" CSS");
			return false;
		}

		String cssSelector=commandParameters[0].trim();

		if(webDriver==null)
		{
			scriptParameters.put("Error", "No browser open");
			return false;
		}

		try
		{
			WebElement element = webDriver.findElement(By.cssSelector(cssSelector));
			String text=element.getAttribute("value");
			scriptParameters.put("Response", text);
			return true;
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Response"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter CSS>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Get a value from a CSS<br/>Usage:<br/>"+COMMAND+" CSS</html>";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
