package custom.selenium;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import eyeautomate.ScriptRunner;

public class SelectText
{
	private static final String COMMAND="SelectText";
	private static final String ICON_FILENAME="icons/insert2.png";
	private WebDriver webDriver=null;

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: SelectText Text");
			return false;
		}

		String text=commandParameters[0].trim();

		if(webDriver==null)
		{
			scriptParameters.put("Error", "No browser open");
			return false;
		}

		try
		{
			WebElement currentElement = webDriver.switchTo().activeElement();
			if(currentElement!=null)
			{
				org.openqa.selenium.support.ui.Select select=new org.openqa.selenium.support.ui.Select(currentElement);
				select.selectByVisibleText(text);
				return true;
			}
			else
			{
				scriptParameters.put("Error", "No active element");
				return false;
			}
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Exception: "+e.toString());
			return false;
		}
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Response"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter Text>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/seleniumcommands.html";
	}

	public String getTooltip()
	{
		return "<html>Select an item in the drop-down or list in focus using a text<br/>Usage:<br/>"+COMMAND+" Text</html>";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		webDriver=(WebDriver)scriptRunner.getWebDriver();
	}
}
