package custom.selenium;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import javax.swing.JOptionPane;

import eyeautomate.ScriptRunner;

public class SetEyeSelLicense
{
	private static final String LICENSE_PROPERTIES_FILE = "settings/license.properties";
	private ScriptRunner scriptRunner=null;

	public String executeTool()
	{
		Properties licensePoperties=loadLicenseProperties();
		String oldLicenseKey=licensePoperties.getProperty("eyesel_license_key", "");
		String licenseKey = JOptionPane.showInputDialog(null, "Enter the EyeSel license key:", oldLicenseKey);
		if(licenseKey!=null)
		{
			licensePoperties.setProperty("eyesel_license_key", licenseKey.trim());
			saveLicenseProperties(licensePoperties);
			if(scriptRunner != null)
			{
				scriptRunner.setRecognitionEngine(null);
			}
//			JOptionPane.showMessageDialog(this, "License key has been saved. Restart "+EYEAUTOMATE_STUDIO+" for the changes to be applied.");
		}
		return null;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private Properties loadLicenseProperties()
	{
		Properties licenseProperties = new Properties();
		try
		{
			FileInputStream in = new FileInputStream(LICENSE_PROPERTIES_FILE);
			licenseProperties.load(in);
			in.close();
		}
		catch (Exception e)
		{
		}
		return licenseProperties;
	}

	private void saveLicenseProperties(Properties licenseProperties)
	{
		try
		{
			FileOutputStream out = new FileOutputStream(LICENSE_PROPERTIES_FILE);
			if (out != null)
			{
				licenseProperties.store(out, null);
				out.close();
			}
		}
		catch (Exception ex)
		{
		}
	}
}
