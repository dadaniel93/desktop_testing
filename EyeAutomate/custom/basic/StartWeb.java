package custom.basic;

import java.awt.Frame;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import eyeautomate.ScriptRunner;

public class StartWeb
{
	private static final String COMMAND="StartWeb";
	private static final String ICON_FILENAME="icons/globe2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Launches the default web browser with the supplied URL<br/>Usage: <br/>"+COMMAND+" URL</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		String text=JOptionPane.showInputDialog("Enter URL", "http://www.");
		if(text==null || text.trim().length()==0)
		{
			return null;
		}

		String url=improveUrl(text);
		if(url==null)
		{
			JOptionPane.showMessageDialog((Frame)null, "Incorrect URL");
			return null;
		}
		try
		{
			new URL(url);
		}
		catch (MalformedURLException ex)
		{
			JOptionPane.showMessageDialog((Frame)null, "Incorrect URL");
			return null;
		}

		return COMMAND+" \"" + url + "\"";
	}

	private String improveUrl(String text)
	{
		String lowercaseText=text.toLowerCase();
		if(lowercaseText.startsWith("http://") || lowercaseText.startsWith("https://"))
		{
			// Correct start
			return text;
		}
		else
		{
			if(lowercaseText.startsWith("http"))
			{
				// Incorrect start
				return null;
			}
			else
			{
				// Add default protocol
				return "http://"+text;
			}
		}
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in StartFile command. Usage: StartFile FilePath", lineNo);
			return false;
		}
		else
		{
			String commandString = commandLine.get(1);
			ScriptRunner.startWeb(commandString);
		}

		return true;
	}
}
