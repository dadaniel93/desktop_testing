package custom.basic;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class MoveRelative extends EyeCommand
{
	private static final String COMMAND="MoveRelative";
	private static final String ICON_FILENAME="icons/move2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(passed)
		{
			scriptRunner.setScreenshot(scriptRunner.captureCurrentLocation());
		}
		else
		{
			scriptRunner.setScreenshot(scriptRunner.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Move the mouse relative to the current position<br/>Usage:<br/>"+COMMAND+" dX dY</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter relative X position>\" \"<String: Enter relative Y position>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
	
	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 3)
		{
			errorLogMessage(scriptFilename, "Missing image parameter in MouseMoveRelative command. Usage: MouseMoveRelative dX dY", lineNo);
			return false;
		}
		else
		{
			String x = commandLine.get(1);
			String y = commandLine.get(2);
			if (isIntString(x) && isIntString(y))
			{
				int dx = string2Int(x);
				int dy = string2Int(y);
				eye.move(new Point(eye.getLastX()+dx, eye.getLastY()+dy));
			}
			else
			{
				errorLogMessage(scriptFilename, "Specify x and y coordinates as integer values", lineNo);
				return false;
			}
		}

		return true;
	}
}
