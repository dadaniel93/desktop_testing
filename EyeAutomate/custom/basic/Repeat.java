package custom.basic;

import java.util.Properties;

import eyeautomate.CustomCommand;
import eyeautomate.ScriptRunner;

public class Repeat
{
	private static final String COMMAND="Repeat";
	private static final String ICON_FILENAME="icons/repeat2.png";
	ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Times/Data/Image/Select");
			return false;
		}

		CustomCommand command=new CustomCommand(COMMAND, commandParameters);
		scriptRunner.setBlockCommand(command);
		scriptRunner.increaseCommandLevel();
		return true;
	}

	public String getTooltip()
	{
		return "<html>Repeat a block<br/>Usage: <br/>"+COMMAND+" Times<br/>"+COMMAND+" Data<br/>"+COMMAND+" SQL Select</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Iteration"};
	}

	public String getCommand()
	{
		return COMMAND+" <String: Enter no times to repeat>...EndRepeat";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	public String getBeginCommand()
	{
		return COMMAND;
	}

	public String getEndCommand()
	{
		return "EndRepeat";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}
}
