package custom.basic;

import java.util.Properties;

import eyeautomate.CustomCommand;
import eyeautomate.ScriptRunner;

public class If
{
	private static final String COMMAND="If";
	private static final String ICON_FILENAME="icons/question2.png";
	ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Image/Expression");
			return false;
		}

		CustomCommand command=new CustomCommand(COMMAND, commandParameters);
		scriptRunner.setBlockCommand(command);
		scriptRunner.increaseCommandLevel();
		return true;
	}

	public String getTooltip()
	{
		return "<html>Perform block if image exist or expression is true<br/>Usage: <br/>"+COMMAND+" Image [Area]<br/>"+COMMAND+" Expression</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" <Image>...EndIf";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	public String getBeginCommand()
	{
		return COMMAND;
	}

	public String getElseCommand()
	{
		return "Else";
	}

	public String getEndCommand()
	{
		return "EndIf";
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}
}
