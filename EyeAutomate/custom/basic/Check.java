package custom.basic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebElement;

import eye.Match;
import eyeautomate.Location;
import eyeautomate.Locations;
import eyeautomate.ScriptRunner;

public class Check extends EyeCommand
{
	private static final String COMMAND="Check";
	private static final String ICON_FILENAME="icons/check2.png";
	private Properties scriptParameters;
	private Point matchLocation=null;
	private boolean isExpression=false;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		matchLocation=null;
		isExpression=false;
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(!isExpression)
		{
			if(passed && matchLocation!=null)
			{
				scriptRunner.setScreenshot(eye.captureLocation((int)matchLocation.getX(), (int)matchLocation.getY()));
			}
			else
			{
				scriptRunner.setScreenshot(eye.getLatestScreenCapture());
			}
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Checks that an image is visible<br/>Waits until the image appears<br/>Usage:<br/>"+COMMAND+" Image [Area]<br/>"+COMMAND+" Text<br/>"+COMMAND+" Expression<br/>"+COMMAND+" Image X Y</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Image>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/basiccommands.html";
	}

	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private BufferedImage loadImage(String filename)
	{
		return scriptRunner.loadImage(filename);
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing image parameter in "+COMMAND+" command. Usage: "+COMMAND+" Image/Expression", lineNo);
			return false;
		}
		else
		{
			if (commandLine.size() == 6)
			{
				String filename = commandLine.get(1);
				if(!(isIntString(commandLine.get(2)) && isIntString(commandLine.get(3)) && isIntString(commandLine.get(4)) && isIntString(commandLine.get(5))))
				{
					errorLogMessage(scriptFilename, "Specify coordinates and size using integer values", lineNo);
					return false;
				}

				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image not found", lineNo);
					return false;
				}

				// Create a target area to check
				int x = string2Int(commandLine.get(2));
				int y = string2Int(commandLine.get(3));
				int width = string2Int(commandLine.get(4));
				int height = string2Int(commandLine.get(5));
				Rectangle targetArea=new Rectangle(x, y, width, height);

				Match match=eye.findImage(imageToFind, targetArea);
				if(match==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}
				else
				{
					scriptParameters.put("LocationX", ""+(int)match.getX());
					scriptParameters.put("LocationY", ""+(int)match.getY());
					matchLocation=match.getCenterLocation();
				}
			}
			else if (ScriptRunner.isWidget(commandLine.get(1)))
			{
				String widgetScriptPath = commandLine.get(1);
				if(!scriptRunner.runWidgetScript(widgetScriptPath))
				{
					scriptParameters.put("Error", "Failed to run Widget script");
					return false;
				}
				
				Locations locations=scriptRunner.getLocations();
				List<Location> sortedLocations=locations.getSortedLocations();
				
				for(Location location:sortedLocations)
				{
					Object unspecifiedElement=location.getElement();
					if(unspecifiedElement!=null && unspecifiedElement instanceof WebElement)
					{
						// A Selenium WebElement
						return true;
					}
					else if(location.getPosition()!=null)
					{
						// Has a position
						Rectangle rect=location.getPosition();
						scriptParameters.put("LocationX", ""+(int)rect.getX());
						scriptParameters.put("LocationY", ""+(int)rect.getY());
						matchLocation=scriptRunner.getVizionEngine().getActualLocation(new Point((int)rect.getCenterX(), (int)rect.getCenterY()));
						return true;
					}
				}

				scriptParameters.put("Error", "Failed to locate Widget");
				return false;
			}
			else if (ScriptRunner.isImage(commandLine.get(1)))
			{
				// One image
				String filename = commandLine.get(1);
				BufferedImage imageToFind = loadImage(filename);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Image not found", lineNo);
					return false;
				}

				Match match=eye.findImage(imageToFind);
				if(match==null)
				{
					errorLogMessage(scriptFilename, command+" failed, image not found", lineNo);
					return false;
				}
				else
				{
					scriptParameters.put("LocationX", ""+(int)match.getX());
					scriptParameters.put("LocationY", ""+(int)match.getY());
					matchLocation=match.getCenterLocation();
				}
			}
			else if(scriptRunner.isExpression(commandLine.get(1)))
			{
				isExpression=true;
				if(!scriptRunner.evaluate(commandLine.get(1)))
				{
					errorLogMessage(scriptFilename, COMMAND+" failed", lineNo);
					return false;
				}
			}
			else
			{
				// One text
				String text = commandLine.get(1);
				String fontName=scriptParameters.getProperty("FontName", "Consolas");
				String fontStyleStr=scriptParameters.getProperty("FontStyle", "Plain");
				int fontStyle=Font.PLAIN;
				if (fontStyleStr.equalsIgnoreCase("bold"))
				{
					fontStyle=Font.BOLD;
				}
				else if (fontStyleStr.equalsIgnoreCase("italic"))
				{
					fontStyle=Font.ITALIC;
				}
				int fontSize=string2Int(scriptParameters.getProperty("FontSize", "11"));
				Font font=eye.createFont(fontName, fontStyle, fontSize, false, false);
				BufferedImage imageToFind = eye.createImageFromText(text, font, Color.BLACK, Color.WHITE);
				if (imageToFind == null)
				{
					errorLogMessage(scriptFilename, "Failed to create image", lineNo);
					return false;
				}

				Match match=eye.findImage(imageToFind);
				if(match==null)
				{
					errorLogMessage(scriptFilename, command+" failed, text not found", lineNo);
					return false;
				}
				else
				{
					scriptParameters.put("LocationX", ""+(int)match.getX());
					scriptParameters.put("LocationY", ""+(int)match.getY());
					matchLocation=match.getCenterLocation();
				}
			}

			return true;
		}
	}
}
