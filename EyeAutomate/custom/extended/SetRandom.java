package custom.extended;

import java.util.Properties;
import java.util.Random;

public class SetRandom
{
	private static final String COMMAND="SetRandom";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<2)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Parameter Value1 [Value2]");
			return false;
		}

		String parameter=commandParameters[0];

		Random generator = new Random(System.currentTimeMillis());
		int randomNumber = generator.nextInt(commandParameters.length-1);
		String parameterValue=commandParameters[randomNumber+1];

		scriptParameters.put(parameter, parameterValue);
		return true;
	}

	public String getTooltip()
	{
		return "<html>Sets a parameter randomly from a number of possible values<br/>Usage: <br/>"+COMMAND+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter parameter>\" \"<String: Enter first value>\" \"<String: Enter second value>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
