package custom.extended;

import java.util.Properties;

import eyeautomate.HttpServiceCaller;

public class IsIdle
{
	private static final String COMMAND="IsIdle";
	private static final String ICON_FILENAME="icons/question2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Address");
			return false;
		}
		try
		{
			String address=commandParameters[0].trim();
			HttpServiceCaller service=new HttpServiceCaller();
			String request=addServerAndCommand(address, "isidle");
			String response=service.executeGetRequest(request);
			if(response==null)
			{
				scriptParameters.put("Error", "Failed to call: "+address);
				return false;
			}
			else
			{
				scriptParameters.put("Response", response);
				return true;
			}
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Call failed: "+e.toString());
			return false;
		}
	}

	private static String addServerAndCommand(String serverAddress, String command)
	{
		String address = serverAddress;
		if (serverAddress.endsWith("/") || serverAddress.endsWith("\\"))
		{
			// Remove trailing slash
			address = serverAddress.substring(0, serverAddress.length() - 1);
		}
		return address + "/" + command;
	}

	public String getTooltip()
	{
		return "Checks if web service is idle";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Response"};
	}
	
	public String getCommand()
	{
		return COMMAND+" \"<String: Enter Address=http://localhost:1234>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
}
