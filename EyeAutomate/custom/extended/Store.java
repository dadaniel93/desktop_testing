package custom.extended;

import java.util.Properties;

public class Store
{
	private static final String COMMAND="Store";
	private static final String ICON_FILENAME="icons/analysis2.png";
	private static Properties store=new Properties();

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Parameter [Value]");
			return false;
		}
		
		String parameter = commandParameters[0];

		if(commandParameters.length==1)
		{
			// Get a value
			String value=store.getProperty(parameter, "Unknown parameter");
			scriptParameters.put(parameter, value);
		}
		else if(commandParameters.length>=2)
		{
			// Set a value
			String value = commandParameters[1];
			store.setProperty(parameter, value);
		}
		return true;
	}

	public String getTooltip()
	{
		return "<html>Store or retrieve a global parameter value<br/>Usage: <br/>"+COMMAND+"Parameter Value<br/>"+COMMAND+"Parameter</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter parameter name to store or retrieve>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
