package custom.extended;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Log
{
	private static final String COMMAND="Log";
	private static final String ICON_FILENAME="icons/insert2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Logs one line to a data file<br/>Usage:<br/>"+COMMAND+" DataFile Delimiter Column1 [Column2]</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Data>\" \"<String: Column delimiter=,|;|:|[TAB]>\" \"<String: Column value>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 4)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Log command. Usage: Log DataFile Delimiter Column1 [Column2]", lineNo);
			return false;
		}
		else
		{
			String filepath = commandLine.get(1);
			String delimiter = commandLine.get(2);
			if("[TAB]".equalsIgnoreCase(delimiter))
			{
				delimiter="\t";
			}
			StringBuffer text=new StringBuffer();
			for (int i = 3; i < commandLine.size(); i++)
			{
				if(text.length()>0)
				{
					text.append(delimiter);
				}
				text.append(commandLine.get(i));
			}
			if(writeLine(filepath, text.toString(), true)==false)
			{
				errorLogMessage(scriptFilename, "Failed to write to the file: "+filepath, lineNo);
				return false;
			}
		}

		return true;
	}

	/**
	 * Write one line of text to filename
	 * @param filename
	 * @param text
	 * @param append
	 */
	private boolean writeLine(String filename, String text, boolean append)
	{
		String logMessage = text + "\r\n";
		File file = new File(filename);
		try
		{
			FileOutputStream o = new FileOutputStream(file, append);
			o.write(logMessage.getBytes());
			o.close();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
