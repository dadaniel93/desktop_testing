package custom.extended;

import java.util.Properties;

import eyeautomate.HttpServiceCaller;
import eyeautomate.ScriptRunner;

public class WaitIdle
{
	private static final String COMMAND="WaitIdle";
	private static final String ICON_FILENAME="icons/hourglass2.png";
	ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<2)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Address TimeoutSeconds");
			return false;
		}
		try
		{
			String address=commandParameters[0].trim();
			String timeoutStr=commandParameters[1].trim();
			int timeout=Integer.parseInt(timeoutStr);
			HttpServiceCaller service=new HttpServiceCaller();
			long startTimestamp=System.currentTimeMillis();
			while(true)
			{
				long currentTimestamp=System.currentTimeMillis();
				if(currentTimestamp>=startTimestamp+timeout*1000)
				{
					scriptParameters.put("Error", "Timeout");
					return false;
				}
				String request=addServerAndCommand(address, "isidle");
				String response=service.executeGetRequest(request);
				if(response==null)
				{
					scriptParameters.put("Error", "Failed to call: "+address);
					return false;
				}
				else
				{
					if("yes".equalsIgnoreCase(response))
					{
						request=addServerAndCommand(address, "getlastreport");
						response=service.executeGetRequest(request);
						if(response!=null)
						{
							String reportLink=addServerAndCommand(address, response);
							scriptRunner.setCommandResultText(reportLink);
						}
						return true;
					}
				}
				try
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e)
				{
					return false;
				}
			}
		}
		catch(Exception e)
		{
			scriptParameters.put("Error", "Call failed: "+e.toString());
			return false;
		}
	}

	private static String addServerAndCommand(String serverAddress, String command)
	{
		String address = serverAddress;
		if (serverAddress.endsWith("/") || serverAddress.endsWith("\\"))
		{
			// Remove trailing slash
			address = serverAddress.substring(0, serverAddress.length() - 1);
		}
		return address + "/" + command;
	}

	public String getTooltip()
	{
		return "Waits until a web service is idle";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "Response"};
	}
	
	public String getCommand()
	{
		return COMMAND+" \"<String: Enter Address=http://localhost:1234>\" <String: Enter seconds to timeout=600>";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}
}
