package custom.extended;

import java.util.Properties;

import eyeautomate.ScriptRunner;

public class Stop
{
	private static final String COMMAND="Stop";
	private static final String ICON_FILENAME="icons/stop2.png";
	ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		scriptRunner.setStop(true);
		return true;
	}

	public String getTooltip()
	{
		return "<html>Stops the run<br/>Usage: <br/>"+COMMAND+"</html>";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
