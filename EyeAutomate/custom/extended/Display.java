package custom.extended;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.DisplayDialog;
import eyeautomate.ScriptRunner;

public class Display
{
	private static final String COMMAND="Display";
	private static final String ICON_FILENAME="icons/monitor2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(!passed)
		{
			scriptRunner.setScreenshot(scriptRunner.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Display texts and images<br/>Any number of texts or images can be displayed<br/>Usage: <br/>"+COMMAND+" [Title] [Text] ...<br/>"+COMMAND+" Image ...</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter a text or image to display>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in Display command. Usage: Display [X] [Y] [Width] [Height] Instruction|ImagePath [Instruction|Image]", lineNo);
			return false;
		}
		else
		{
			int xValue=-1;
			int yValue=-1;
			int widthValue = 380;
			int heightValue = 240;
			boolean hasModifiedSize=false;
			int indexToFirstParameter=1;
			if (commandLine.size()>=5 && isIntString(commandLine.get(1)) && isIntString(commandLine.get(2)) && isIntString(commandLine.get(3)) && isIntString(commandLine.get(4)))
			{
				// First 4 are integers - x, y, width, height
				xValue = string2Int(commandLine.get(1));
				yValue = string2Int(commandLine.get(2));
				widthValue = string2Int(commandLine.get(3));
				heightValue = string2Int(commandLine.get(4));
				indexToFirstParameter=5;
				hasModifiedSize=true;
			}
			else if (commandLine.size()>=3 && isIntString(commandLine.get(1)) && isIntString(commandLine.get(2)))
			{
				// First 2 are integers width, height
				widthValue = string2Int(commandLine.get(1));
				heightValue = string2Int(commandLine.get(2));
				indexToFirstParameter=3;
				hasModifiedSize=true;
			}

			String displayTitle=null;
			List<String> images=new ArrayList<String>();
			for (int i = indexToFirstParameter; i < commandLine.size(); i++)
			{
				String param=commandLine.get(i);
				if(ScriptRunner.isImage(param))
				{
					// An image
					images.add("<img src='" + param + "'/>");
					if(!hasModifiedSize && i == indexToFirstParameter)
					{
						// Has not specified size and first parameter is an image
						BufferedImage image=scriptRunner.loadImage(param);
						if (image == null)
						{
							errorLogMessage(scriptFilename, "Image/Text not found", lineNo);
							return false;
						}
						widthValue=image.getWidth();
						heightValue=image.getHeight();
					}
				}
				else
				{
					// A text
					if(i==indexToFirstParameter)
					{
						// First parameter is the title
						if(commandLine.size()==indexToFirstParameter+1)
						{
							// Only one parameter
							displayTitle="Display";
							images.add(param);
						}
						else
						{
							displayTitle=param;
						}
					}
					else
					{
						images.add(param);
					}
				}
			}
			DisplayDialog dialog=null;
			if(xValue!=-1 && yValue!=-1)
			{
				dialog=new DisplayDialog(displayTitle, xValue, yValue, widthValue, heightValue);
			}
			else
			{
				dialog=new DisplayDialog(displayTitle, widthValue, heightValue);
			}
			dialog.setScriptRunner(scriptRunner);
			dialog.showDialog(images);
			return true;
		}
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
