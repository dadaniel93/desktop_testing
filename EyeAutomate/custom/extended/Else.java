package custom.extended;

import java.util.Properties;

import eyeautomate.Command;
import eyeautomate.CustomCommand;
import eyeautomate.ScriptRunner;

public class Else
{
	private static final String COMMAND="Else";
	private static final String ICON_FILENAME="icons/question2.png";
	ScriptRunner scriptRunner=null;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		Command command=(Command)scriptRunner.getBlockCommand();
		if(command instanceof CustomCommand)
		{
			CustomCommand customCommand=(CustomCommand)command;
			String commandName=customCommand.getCommand();
			if(!(commandName.equalsIgnoreCase("if") || commandName.equalsIgnoreCase("ifnot")))
			{
				scriptParameters.put("Error", COMMAND+" should follow an If or IfNot");
				return false;
			}
		}
		return true;
	}

	public String getTooltip()
	{
		return "<html>Perform block if image exist or expression is true<br/>Usage: <br/>"+COMMAND+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}
}
