package custom.extended;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import custom.basic.EyeCommand;
import eye.Match;
import eyeautomate.ScriptRunner;

public class Capture extends EyeCommand
{
	private static final String COMMAND="Capture";
	private static final String ICON_FILENAME="icons/capture2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Captures an image<br/>Usage:<br/>"+COMMAND+"<br/>"+COMMAND+" ScreenNo<br/>"+COMMAND+" Image Area<br/>"+COMMAND+" Width Height<br/>"+COMMAND+" X Y Width Height<br/></html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", "CapturedImage"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Image>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
	
	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private BufferedImage loadImage(String filename)
	{
		return scriptRunner.loadImage(filename);
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() == 1)
		{
			// Capture the whole screen
			BufferedImage latestCapture = eye.createScreenCapture();
			saveScreenshot("CapturedImage", latestCapture);
		}
		else if (commandLine.size() == 3)
		{
			String width = commandLine.get(1);
			String height = commandLine.get(2);
			if (!isIntString(width) || !isIntString(height))
			{
				errorLogMessage(scriptFilename, "Specify Width and Height as integer values", lineNo);
				return false;
			}
			int widthValue = string2Int(width);
			int heightValue = string2Int(height);
			BufferedImage latestCapture=eye.captureLocation(eye.getLastX(), eye.getLastY(), widthValue, heightValue);
			saveScreenshot("CapturedImage", latestCapture);
		}
		else if (commandLine.size() == 5)
		{
			String x = commandLine.get(1);
			String y = commandLine.get(2);
			String width = commandLine.get(3);
			String height = commandLine.get(4);
			if (!isIntString(x) || !isIntString(y) || !isIntString(width) || !isIntString(width))
			{
				errorLogMessage(scriptFilename, "Specify X, Y, Width and Height as integer values", lineNo);
				return false;
			}
			int xValue = string2Int(x);
			int yValue = string2Int(y);
			int widthValue = string2Int(width);
			int heightValue = string2Int(height);
			BufferedImage latestCapture = eye.createScreenCapture();
			BufferedImage subImage=eye.getSubimage(latestCapture, xValue, yValue, widthValue, heightValue);
			saveScreenshot("CapturedImage", subImage);
		}
		else if (commandLine.size() == 6)
		{
			String filename = commandLine.get(1);
			BufferedImage imageToFind = loadImage(filename);
			if (imageToFind == null)
			{
				errorLogMessage(scriptFilename, "Image/Text not found", lineNo);
				return false;
			}

			String dX = commandLine.get(2);
			String dY = commandLine.get(3);
			String width = commandLine.get(4);
			String height = commandLine.get(5);
			if (!isIntString(dX) || !isIntString(dY) || !isIntString(width) || !isIntString(width))
			{
				errorLogMessage(scriptFilename, "Specify dX, dY, Width and Height as integer values", lineNo);
				return false;
			}
			int dXValue = string2Int(dX);
			int dYValue = string2Int(dY);
			int widthValue = string2Int(width);
			int heightValue = string2Int(height);

			Rectangle removedSelection=new Rectangle(dXValue, dYValue, widthValue, heightValue);
			eye.setRemovedSelection(removedSelection);
			Match match=eye.findImage(imageToFind);
			eye.resetRemovedSelection();
			if(match==null)
			{
				errorLogMessage(scriptFilename, "Capture failed, image not found", lineNo);
				return false;
			}

			BufferedImage latestCapture = eye.createScreenCapture();
			int x=match.getX()+dXValue;
			int y=match.getY()+dYValue;
			BufferedImage subImage=eye.getSubimage(latestCapture, x, y, widthValue, heightValue);
			saveScreenshot("CapturedImage", subImage);
		}
		else
		{
			errorLogMessage(scriptFilename, "Missing image parameter in Capture command. Usage: Capture [Image] [X] [Y] [dX] [dY] [Width] [Height] [ScreenNo]", lineNo);
			return false;
		}

		return true;
	}

	private void saveScreenshot(String filename, BufferedImage capture)
	{
		String filepath = scriptRunner.addRootFolder(scriptRunner.getImageFolder()+"/" + System.currentTimeMillis() + ".png");
		scriptRunner.savePngImage(capture, filepath);
		String relativeFilePath=ScriptRunner.getRelativeFilePath(filepath);
		scriptRunner.setParameter(filename, relativeFilePath);
		scriptRunner.setScreenshot(capture);
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
