package custom.extended;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ClearLog
{
	private static final String COMMAND="ClearLog";
	private static final String ICON_FILENAME="icons/delete2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Clears a data file<br/>Usage:<br/>"+COMMAND+" DataFile</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Data>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in ClearLog command. Usage: ClearLog DataFile", lineNo);
			return false;
		}
		else
		{
			String filepath = commandLine.get(1);
			if(saveTextFile(filepath, "")==false)
			{
				errorLogMessage(scriptFilename, "Failed to write to the file: "+filepath, lineNo);
				return false;
			}
		}

		return true;
	}

	/**
	 * Save a text file
	 * @param filename
	 * @param text
	 * @return true if saved
	 */
	private static boolean saveTextFile(String filename, String text)
	{
		if (text == null)
		{
			return false;
		}

		File file=new File(filename);
		if(file!=null && file.exists() && !file.canWrite())
		{
			// Write protected
			return false;
		}

		FileOutputStream o = null;
		try
		{
			o = new FileOutputStream(filename, false);
			FileDescriptor fd = o.getFD();
			o.write(text.getBytes("UTF-8"));
			o.flush();
			fd.sync();
		}
		catch (Exception e)
		{
			return false;
		}
		finally
		{
			try
			{
				if (o != null)
					o.close();
			}
			catch (Exception e)
			{
			}
		}
		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/extendedcommands.html";
	}
}
