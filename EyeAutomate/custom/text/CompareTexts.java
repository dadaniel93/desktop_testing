package custom.text;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public class CompareTexts
{
	private static final String COMMAND="CompareTexts";
	private static final String SETS_PARAMETER="Similarity";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<2)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Text1 Text2");
			return false;
		}

		String text1=commandParameters[0];
		String text2=commandParameters[1];

		Hashtable<String, String> pairs1=createPairs(text1);
		Hashtable<String, String> pairs2=createPairs(text2);
		int match=match(pairs1, pairs2);
		
		scriptParameters.put(SETS_PARAMETER, ""+match);
		return true;
	}

	public String getTooltip()
	{
		return "<html>Compares two texts and results the "+SETS_PARAMETER+" in percent<br/>Usage: <br/>"+COMMAND+" Text1 Text2<br/>Sets parameter: "+SETS_PARAMETER+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", SETS_PARAMETER};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter the first text>\" \"<String: Enter the second text>\"";
	}
	
	private Hashtable<String, String> createPairs(String text)
	{
		Hashtable<String, String> hashtable=new Hashtable<String, String>();
		
		String lowercase=text.toLowerCase();
		for(int i=0; i<lowercase.length()-1; i++)
		{
			char first=text.charAt(i);
			char second=text.charAt(i+1);
			if((Character.isAlphabetic(first) || Character.isDigit(first)) && (Character.isAlphabetic(second) || Character.isDigit(second)))
			{
				String substring=""+first+second;
				hashtable.put(substring, substring);
			}
		}
		
		return hashtable;
	}
	
	private int match(Hashtable<String, String> hashtable1, Hashtable<String, String> hashtable2)
	{
		int noElements=0;
		int noMatches=0;
		Enumeration<String> keys=hashtable1.keys();
		while(keys.hasMoreElements())
		{
			noElements++;
			String key=keys.nextElement();
			if(hashtable2.containsKey(key))
			{
				noMatches++;
			}
		}
		if(noElements==0)
		{
			return 0;
		}
		int percent=(noMatches*100)/noElements;
		return percent;
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
}
