package custom.text;

import java.util.Properties;

public class StartsWith
{
	private static final String COMMAND="StartsWith";
	private static final String SETS_PARAMETER="StartsWith";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<2)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Text StartText");
			return false;
		}

		String text=commandParameters[0];
		String startText=commandParameters[1];
		if(text.startsWith(startText))
		{
			scriptParameters.put(SETS_PARAMETER, "Yes");
		}
		else
		{
			scriptParameters.put(SETS_PARAMETER, "No");
		}
		return true;
	}

	public String getTooltip()
	{
		return "<html>Determines if a text starts with a given string<br/>Usage: <br/>"+COMMAND+" Text StartText<br/>Sets parameter: "+SETS_PARAMETER+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", SETS_PARAMETER};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter the text>\" \"<String: Enter the starting text>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
}
