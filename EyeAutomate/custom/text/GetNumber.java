package custom.text;

import java.util.Properties;

public class GetNumber
{
	private static final String COMMAND="GetNumber";
	private static final String SETS_PARAMETER="Number";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Text");
			return false;
		}

		String text=commandParameters[0];
		String digits="";
		for(int i=0; i<text.length(); i++)
		{
			char c=text.charAt(i);
			if(Character.isDigit(c) || c=='.' || c==',')
			{
				digits+=c;
			}
		}
		scriptParameters.put(SETS_PARAMETER, digits);
		return true;
	}

	public String getTooltip()
	{
		return "<html>Extracts a decimal number from a text<br/>Usage: <br/>"+COMMAND+" Text<br/>Sets parameter: "+SETS_PARAMETER+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", SETS_PARAMETER};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter a text>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
}
