package custom.text;

import java.util.Properties;

public class Trim
{
	private static final String COMMAND="Trim";
	private static final String SETS_PARAMETER="Trimmed";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Text");
			return false;
		}

		String text=commandParameters[0];

		scriptParameters.put(SETS_PARAMETER, text.trim());
		return true;
	}

	public String getTooltip()
	{
		return "<html>Trims the text from whitespace<br/>Usage: <br/>"+COMMAND+" Text<br/>Sets parameter: "+SETS_PARAMETER+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error", SETS_PARAMETER};
	}
	
	public String getCommand()
	{
		return COMMAND+" \"<String: Enter a text>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
}
