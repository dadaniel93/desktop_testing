package custom.mouse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import eyeautomate.ScriptRunner;

public class MouseScroll
{
	private static final String COMMAND="MouseScroll";
	private static final String ICON_FILENAME="icons/mouse2.png";
	private ScriptRunner scriptRunner=null;
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		if(passed)
		{
			scriptRunner.setScreenshot(scriptRunner.captureCurrentLocation());
		}
		else
		{
			scriptRunner.setScreenshot(scriptRunner.getLatestScreenCapture());
		}
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Scrolls the mouse wheel a number of steps<br/>Usage:<br/>"+COMMAND+" Steps</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter number of steps to scroll (pos or neg)>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}
	
	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	public void setScriptRunner(ScriptRunner scriptRunner)
	{
		this.scriptRunner=scriptRunner;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in MouseScroll command. Usage: MouseScroll Steps", lineNo);
			return false;
		}
		else
		{
			String sensitivityStr = commandLine.get(1);
			if (isIntString(sensitivityStr))
			{
				// Wait a number of milliseconds
				int steps = string2Int(sensitivityStr);
				if (steps < -100 || steps > 100)
				{
					errorLogMessage(scriptFilename, "Specify a value between -100 and 100", lineNo);
					return false;
				}
				else
				{
					scriptRunner.mouseScroll(steps);
				}
			}
			else
			{
				errorLogMessage(scriptFilename, "Specify a value between -100 and 100", lineNo);
				return false;
			}
		}

		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/mousecommands.html";
	}
}
