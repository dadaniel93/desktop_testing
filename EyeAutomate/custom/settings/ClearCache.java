package custom.settings;

import java.util.Properties;

import custom.basic.EyeCommand;

public class ClearCache extends EyeCommand
{
	private static final String COMMAND="ClearCache";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		eye.clearBoxesCache();
		eye.clearMatchLocationCache();
		return true;
	}

	public String getTooltip()
	{
		return "<html>Clears the boxes and match location cache<br/>Usage:<br/>"+COMMAND+"</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/settingscommands.html";
	}
}
