package custom.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import custom.basic.EyeCommand;

public class CpuUsage extends EyeCommand
{
	private static final String COMMAND="CpuUsage";
	private static final String ICON_FILENAME="icons/analysis2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		boolean passed=performCommand("", COMMAND, 1, commandLine);
		return passed;
	}

	public String getTooltip()
	{
		return "<html>Adjust the maximum CPU usage<br/>Usage: <br/>"+COMMAND+" Percent</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter percent value>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() < 2)
		{
			errorLogMessage(scriptFilename, "Missing parameter in CpuUsage command. Usage: CpuUsage Percent", lineNo);
			return false;
		}
		else
		{
			String sensitivityStr = commandLine.get(1);
			if (isIntString(sensitivityStr))
			{
				int cpuUsagePercent = string2Int(sensitivityStr);
				if (cpuUsagePercent < 1 || cpuUsagePercent > 100)
				{
					errorLogMessage(scriptFilename, "Specify a CpuUsage between 1 and 100", lineNo);
					return false;
				}
				else
				{
					eye.setCpuUsage(cpuUsagePercent);
				}
			}
			else
			{
				errorLogMessage(scriptFilename, "Specify a CpuUsage between 1 and 100", lineNo);
				return false;
			}
		}

		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/settingscommands.html";
	}
}
