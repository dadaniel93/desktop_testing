package custom.settings;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import custom.basic.EyeCommand;

public class Region extends EyeCommand
{
	private static final String COMMAND="Region";
	private static final String ICON_FILENAME="icons/analysis2.png";
	private Properties scriptParameters;
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		this.scriptParameters=scriptParameters;
		List<String> commandLine=new ArrayList<String>(Arrays.asList(commandParameters));
		commandLine.add(0, COMMAND);
		return performCommand("", COMMAND, 1, commandLine);
	}

	public String getTooltip()
	{
		return "<html>Limits the region used when finding images<br/>Usage:<br/>"+COMMAND+"<br/>"+COMMAND+" X Y Width Height<br/></html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Enter X>\" \"<String: Enter Y>\" \"<String: Enter Width>\" \"<String: Enter Height>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	/**
	 * Determine if s is an integer (may begin with a - or + sign)
	 * @param s
	 * @return true if s is an integer
	 */
	private static boolean isIntString(String s)
	{
		String text = s.trim();
		for (int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			if (i==0)
			{
				// First char may contain - or +
				if (!(Character.isDigit(c) || c == '-' || c == '+'))
				{
					return false;
				}
			}
			else
			{
				if (!Character.isDigit(c))
				{
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Converts to s to an int
	 * @return An int or 0 if failed
	 */
	private static int string2Int(String s)
	{
		return string2Int(s, 0);
	}

	/**
	 * Converts to s to an int
	 * @return An int or an alternative int if failed
	 */
	private static int string2Int(String s, int otherwise)
	{
		String text = s.trim();
		if(s.length()==0)
		{
			// Blank
			return 0;
		}
		char c = text.charAt(0);
		if (c == '+')
		{
			// Remove + sign
			text=text.substring(1);
		}
		try
		{
			return Integer.parseInt(text);
		}
		catch (Exception e)
		{
			// Couldn't convert to int
			return otherwise;
		}
	}

	private void errorLogMessage(String scriptFilename, String text, int lineNo)
	{
		scriptParameters.put("Error", text);
	}

	private boolean performCommand(String scriptFilename, String command, int lineNo, List<String> commandLine)
	{
		if (commandLine.size() == 1)
		{
			// Capture the whole screen
			eye.setRegion(null);
		}
		else if (commandLine.size() == 5)
		{
			String x = commandLine.get(1);
			String y = commandLine.get(2);
			String width = commandLine.get(3);
			String height = commandLine.get(4);
			if (!isIntString(x) || !isIntString(y) || !isIntString(width) || !isIntString(width))
			{
				errorLogMessage(scriptFilename, "Specify X, Y, Width and Height as integer values", lineNo);
				return false;
			}
			int xValue = string2Int(x);
			int yValue = string2Int(y);
			int widthValue = string2Int(width);
			int heightValue = string2Int(height);
			Rectangle region=new Rectangle(xValue, yValue, widthValue, heightValue);
			eye.setRegion(region);
		}
		else
		{
			errorLogMessage(scriptFilename, "Missing image parameter in Region command. Usage: Region [X] [Y] [Width] [Height]", lineNo);
			return false;
		}

		return true;
	}
}
