package custom.settings;

import java.util.Properties;

import custom.basic.EyeCommand;

public class UseMatchCache extends EyeCommand
{
	private static final String COMMAND="UseMatchCache";
	private static final String ICON_FILENAME="icons/analysis2.png";
	
	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in "+COMMAND+" command. Usage: "+COMMAND+" Yes/No");
			return false;
		}

		String param = commandParameters[0];
		if (param.equalsIgnoreCase("no"))
		{
			eye.setUseMatchCache(false);
		}
		else if (param.equalsIgnoreCase("yes"))
		{
			eye.setUseMatchCache(true);
		}
		else
		{
			scriptParameters.put("Error", "Invalid option, specify Yes or No");
			return false;
		}

		return true;
	}

	public String getTooltip()
	{
		return "<html>Enable or disable the match location cache<br/>Usage: <br/>"+COMMAND+" Yes/No</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<Confirm>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/settingscommands.html";
	}
}
