package custom.settings;

import java.util.Properties;

import custom.basic.EyeCommand;
import eye.Eye;

public class RecognitionMode extends EyeCommand
{
	private static final String COMMAND="RecognitionMode";
	private static final String ICON_FILENAME="icons/analysis2.png";

	public Boolean executeCommand(String[] commandParameters, Properties scriptParameters)
	{
		if(commandParameters.length<1)
		{
			scriptParameters.put("Error", "Missing parameter in command. Usage: "+COMMAND+" Tolerant/Color/Exact");
			return false;
		}
		String param = commandParameters[0].trim();
		if(!setRecognitionMode(param))
		{
			scriptParameters.put("Error", "Invalid image recognition mode, specify Tolerant, Color or Exact");
			return false;
		}

		return true;
	}

	public String getTooltip()
	{
		return "<html>Select the image recognition mode<br/>Usage:<br/>"+COMMAND+" Tolerant/Color/Exact</html>";
	}

	public String[] getParameters()
	{
		return new String[]{"Error"};
	}

	public String getCommand()
	{
		return COMMAND+" \"<String: Select image recognition mode=Tolerant|Color|Exact>\"";
	}

	public String getIconFilename()
	{
		return ICON_FILENAME;
	}

	private boolean setRecognitionMode(String recognitionMode)
	{
		if("tolerant".equalsIgnoreCase(recognitionMode))
		{
			eye.setRecognitionMode(Eye.RecognitionMode.TOLERANT);
		}
		else if("color".equalsIgnoreCase(recognitionMode))
		{
			eye.setRecognitionMode(Eye.RecognitionMode.COLOR);
		}
		else if("exact".equalsIgnoreCase(recognitionMode))
		{
			eye.setRecognitionMode(Eye.RecognitionMode.EXACT);
		}
		else
		{
			return false;
		}
		return true;
	}

	public String getHelp()
	{
		return "http://www.eyeautomate.com/settingscommands.html";
	}
}
